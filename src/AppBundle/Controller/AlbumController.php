<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Album;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AlbumController extends Controller
{
	/**
	 * @param Request $request
	 * @param Album $album
	 * @return \Symfony\Component\HttpFoundation\Response
	 * @Route("update", name="tracks_update")
	 */
	public function updateTracksAction(Request $request, Album $album)
	{
		$form = $this->createForm(ChangeAlbumTracksType::class, $album);
		$form->handleRequest($request);
		
		if ($form->isSubmitted() && $form->isValid()) {
			// Save here
		}
		
		return $this->render('template.html.twig', [
			'form' => $form->createView(),
		]);
	}
}
