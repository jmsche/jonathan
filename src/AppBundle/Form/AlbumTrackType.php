<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AlbumTrackType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add(
				'track',
				EntityType::class,
				[
					'class'        => Track::class,
					'choice_label' => 'name',
				]
			)
			->add('position', IntegerType::class);
	}
	
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefault('data_class', Album::class);
	}
	
	public function getBlockPrefix()
	{
		return 'app_bundle_album_track_type';
	}
}
