<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChangeAlbumTracksType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder->add(
			'relatedTracks',
			CollectionType::class,
			[
				'entry_type'   => AlbumTrackType::class,
				'allow_add'    => true,
				'by_reference' => false,
			]
		);
	}
	
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefault('data_class', Album::class);
	}
	
	public function getBlockPrefix()
	{
		return 'app_bundle_change_album_tracks_type';
	}
}
