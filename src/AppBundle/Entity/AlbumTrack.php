<?php


namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * This is the relation between an album and a track.
 * @ORM\Entity
 * @ORM\Table(name="album_track")
 */
class AlbumTrack
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Column(type="integer")
	 */
	private $id;
	
	/**
	 * @ORM\ManyToOne(targetEntity="Track", inversedBy="relatedAlbums")
	 * @ORM\Column(type="string")
	 */
	private $track;
	
	/**
	 * @ORM\ManyToOne(targetEntity="Album", inversedBy="relatedTracks")
	 * @ORM\Column(type="string")
	 */
	private $album;
	
	/**
	 * @ORM\Column(type="integer")
	 */
	private $position;
	
	/**
	 * @return mixed
	 */
	public function getTrack()
	{
		return $this->track;
	}
	
	/**
	 * @param mixed $track
	 */
	public function setTrack($track)
	{
		$this->track = $track;
	}
	
	/**
	 * @return mixed
	 */
	public function getAlbum()
	{
		return $this->album;
	}
	
	/**
	 * @param mixed $album
	 */
	public function setAlbum($album)
	{
		$this->album = $album;
	}
	
	/**
	 * @return mixed
	 */
	public function getPosition()
	{
		return $this->position;
	}
	
	/**
	 * @param mixed $position
	 */
	public function setPosition($position)
	{
		$this->position = $position;
	}
	
	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}
	
	
}