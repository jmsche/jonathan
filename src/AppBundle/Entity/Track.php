<?php


namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * A track is related to several albums, but it has some extra properties - like position - so OneToMany
 * @ORM\Entity()
 * @ORM\Table()
 */
class Track
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Column(type="integer")
	 */
	private $id;
	/**
	 * @ORM\Column(type="string")
	 */
	private $trackName;
	
	/**
	 * @ORM\OneToMany(targetEntity="AlbumTrack", mappedBy="track")
	 * @ORM\Column(type="string")
	 */
	private $relatedAlbums;
	
	public function __construct()
	{
		$this->relatedAlbums = new ArrayCollection();
	}
	
	public function getRelatedAlbums()
	{
		return $this->relatedAlbums;
	}
	
	public function setRelatedAlbums($albums)
	{
		$this->relatedAlbums = $albums;
	}
	
	/**
	 * @return mixed
	 */
	public function getTrackName()
	{
		return $this->trackName;
	}
	
	/**
	 * @param mixed $trackName
	 */
	public function setTrackName($trackName)
	{
		$this->trackName = $trackName;
	}

	
	
	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}
	
	
}