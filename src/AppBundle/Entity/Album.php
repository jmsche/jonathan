<?php


namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * An album is related to several tracks, but tracks have some extra properties - like position - so OneToMany
 * @ORM\Entity()
 * @ORM\Table()
 */
class Album
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Column(type="integer")
	 */
	private $id;
	/**
	 * @ORM\OneToMany(targetEntity="AlbumTrack", mappedBy="album", cascade={"all"})
	 * @ORM\Column(type="string")
	 */
	private $relatedTracks;
	
	public function __construct()
	{
		$this->relatedTracks = new ArrayCollection();
	}
	
	public function getRelatedTracks()
	{
		return $this->relatedTracks;
	}
	
	public function setRelatedTracks($tracks)
	{
		$this->relatedTracks = $tracks;
	}
	
	public function addRelatedTrack($track)
	{
		if (!$this->relatedTracks->contains($track)) {
			$this->relatedTracks->add($track);
			$track->setAlbum($this);
		}
	}
	
	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}
	
	
}